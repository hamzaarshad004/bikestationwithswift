//
//  DetailTableViewCell.swift
//  BikeStationWithSwift
//
//  Created by ladmin on 05/06/2020.
//  Copyright © 2020 hamza. All rights reserved.
//

import UIKit
import CoreData

class DetailTableViewCell: UITableViewCell {

    @IBOutlet weak var lblStatusValue: UILabel!
    @IBOutlet weak var swtStatus: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setTableCells(stationDetail: NSManagedObject) {
        
        self.lblStatusValue.text = stationDetail.value(forKey: STATUS_VALUE) as! String
       
    }


}
