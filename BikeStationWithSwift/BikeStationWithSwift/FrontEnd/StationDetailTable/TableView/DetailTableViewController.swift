//
//  DetailTableViewController.swift
//  BikeStationWithSwift
//
//  Created by ladmin on 05/06/2020.
//  Copyright © 2020 hamza. All rights reserved.
//

import UIKit
import CoreData

class DetailTableViewController: UITableViewController {

    @IBOutlet var mytableView: UITableView!
    
    var bike: NSManagedObject?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        self.mytableView.estimatedRowHeight = 100.0;
        self.mytableView.rowHeight = UITableView.automaticDimension;
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:DetailTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TableCell", for: indexPath) as! DetailTableViewCell
        cell.setTableCells(stationDetail: bike!)

        return cell
    }
    
    
    @IBAction func statusChange(_ sender: Any) {
        CoreDataManager.getCoreDataManager.changeStationStatus(station: self.bike!)
        self.mytableView.reloadData()
    }
    
}
