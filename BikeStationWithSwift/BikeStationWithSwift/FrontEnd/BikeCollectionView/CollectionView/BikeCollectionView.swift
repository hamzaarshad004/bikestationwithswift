//
//  BikeCollectionView.swift
//  BikeStationWithSwift
//
//  Created by ladmin on 04/06/2020.
//  Copyright © 2020 hamza. All rights reserved.
//

import UIKit
import CoreData
import SwiftSpinner

private let reuseIdentifier = "Station"

class BikeCollectionView: UICollectionViewController {
    
    // MARK - IBOutlets
    
    @IBOutlet var colView: UICollectionView!
    @IBOutlet weak var refreshButton: UIBarButtonItem!
    
    var json = NSDictionary()
    var stations = [Any]()
    var bikes = [NSManagedObject]()


    
    override func viewDidAppear(_ animated: Bool){
        super.viewDidAppear(animated)
        showSpinner(loadingState: 1)
        getBikeList()
    }
    
    // MARK - Data Update Function
    
    func getBikeList() -> () {
        bikes.removeAll()
        
        bikes = CoreDataManager.getCoreDataManager.getStationList() as! [NSManagedObject]
        //print(bikes.count)
        DispatchQueue.main.async {
            self.colView.reloadData()
            self.showSpinner(loadingState: 0)
        }
    }
    
    func getBikeFromAPI(completionBlock: @escaping ()->()) -> () {
        APIManager.getAPIManager.getBikeStations(){
            expectedDictionary in
            self.json = expectedDictionary
            self.stations = expectedDictionary[STATIONS_LIST_NAME] as! [Any]
            //print(self.stations)
            completionBlock();
        }
    }
    
    func populateCoreData() -> () {
        getBikeFromAPI(){
            () in
            CoreDataManager.getCoreDataManager.populateStationList(stations: self.stations)
            self.getBikeList()
        }
    }
    
    @IBAction func refreshCoreData(_ sender: Any) {
        DispatchQueue.main.async {
            self.showSpinner(loadingState: 1)
        }
        populateCoreData()
    }
    
    @IBAction func deleteBikeStation(_ sender: UIButton) {
        
        let cell:StationViewCell = sender.superview?.superview as! StationViewCell
        let indexPath:NSIndexPath = self.colView.indexPath(for: cell) as! NSIndexPath
        CoreDataManager.getCoreDataManager.deleteStation(station: bikes[indexPath.row])
        getBikeList()
    }
    
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showStationDetail") {
            let indexPaths = collectionView.indexPathsForSelectedItems
            let destViewController = segue.destination as? DetailTableViewController
            let indexPath = indexPaths?[0]
            destViewController?.bike = bikes[indexPath?.row ?? 0]
            //destViewController?.index = indexPath?.row
            if let indexPath = indexPath {
                collectionView.deselectItem(at: indexPath, animated: false)
            }
        }
    }
    

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return bikes.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) ->
        UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! StationViewCell
        
        // Configure the cell
        cell.updateCells(station: bikes[indexPath.row])
    
        return cell
    }

    // MARK: UICollectionViewDelegate
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {

        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let reusableview = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "HeaderView", for: indexPath) as! CollectionViewHeader
               reusableview.headerView.text = "Stations and their Counts"
             //do other header related calls or settups
                return reusableview


        default:  fatalError("Unexpected element kind")
        }
    }
    
    func showSpinner(loadingState: Int) -> () {
        if (loadingState == 1){
            refreshButton.isEnabled = false
            colView.alpha = 0.5
            colView.isScrollEnabled = false
            SwiftSpinner.show("Loading data")
        }
        else if (loadingState == 0){
            refreshButton.isEnabled = true
            colView.alpha = 1.0
            colView.isScrollEnabled = true
            SwiftSpinner.hide()
        }
    }
}
