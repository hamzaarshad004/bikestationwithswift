//
//  CollectionViewHeader.swift
//  BikeStationWithSwift
//
//  Created by ladmin on 04/06/2020.
//  Copyright © 2020 hamza. All rights reserved.
//

import UIKit

class CollectionViewHeader: UICollectionReusableView {
        
    @IBOutlet weak var headerView: UILabel!
}
