//
//  StationViewCell.swift
//  BikeStationWithSwift
//
//  Created by ladmin on 04/06/2020.
//  Copyright © 2020 hamza. All rights reserved.
//

import UIKit
import CoreData

class StationViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblStationName: UILabel!
    @IBOutlet weak var lblAvailableDocks: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    
    public func updateCells(station: NSManagedObject){
        self.lblStationName.text = (station.value(forKey:STATION_NAME) as! String)
        self.lblAvailableDocks.text = String(format: "%@", station.value(forKey:AVAILABLE_DOCKS) as! CVarArg)
    }
}
