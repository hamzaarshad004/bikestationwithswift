//
//  CoreDataManager.swift
//  BikeStationWithSwift
//
//  Created by ladmin on 04/06/2020.
//  Copyright © 2020 hamza. All rights reserved.
//

import UIKit
import CoreData

class CoreDataManager: NSObject {
    
    static let getCoreDataManager = CoreDataManager()
    
    private override init(){}
    
    // MARK: - BikeList Entity Functions
    
    func deleteAll() -> () {
        let managedContext = persistentContainer.viewContext
        
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: BIKE_LIST)
        let request = NSBatchDeleteRequest(fetchRequest: fetch)
        
        do {
            try managedContext.execute(request)
        } catch {
            print(error)
        }
        
        do {
            try managedContext.save()
        } catch  {
            print(error)
        }
    }
    
    func populateStationList(stations: Array<Any>){
        
        deleteAll()
        print(getStationList().count)
        
        let managedContext = persistentContainer.viewContext
        for i in 0...stations.count - 1{
            let Item =  NSEntityDescription.entity(forEntityName: BIKE_LIST, in: managedContext)
            let station = Station(entity: Item!, insertInto: managedContext)
            station.identity = (stations[i] as AnyObject).value(forKey: "id") as! NSInteger
            station.availableDocks = (stations[i] as AnyObject).value(forKey: AVAILABLE_DOCKS) as! NSInteger
            station.stationName = (stations[i] as AnyObject).value(forKey: STATION_NAME) as! String
            station.statusKey = (stations[i] as AnyObject).value(forKey: STATUS_KEY) as! NSInteger
            station.statusValue = (stations[i] as AnyObject).value(forKey: STATUS_VALUE) as! String
            
        }
        
        do {
            try managedContext.save()
        }
        catch let error as NSError {
            print ("Could not save. \(error). \(error.userInfo)")
        }
    }
    
    func getStationList() -> Array<Any> {
        let managedContext = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName:BIKE_LIST)
        fetchRequest.predicate = NSPredicate(format:"%K == %@", STATUS_VALUE, STATUS_VALUE_IN_SERVICE)
        do {
            let result = try managedContext.fetch(fetchRequest) as! [NSManagedObject]
            //print(result.count)
            return result
        } catch  {
            print("Failed")
            return []
        }
    }
    
    func deleteStation(station: NSManagedObject) -> () {
        let managedContext = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName:BIKE_LIST)
        let stationId:NSNumber = station.value(forKey: IDENTITY) as! NSNumber
        fetchRequest.predicate = NSPredicate(format:"%K == %@", IDENTITY, stationId)
        do {
            let result = try managedContext.fetch(fetchRequest)
            let object = result[0] as! NSManagedObject
            managedContext.delete(object)
            
            do {
                try managedContext.save()
            } catch  {
                print(error)
            }
           
        } catch  {
            print("Failed")
        }
    }
    
    func changeStationStatus(station: NSManagedObject) -> () {
        let managedContext = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName:BIKE_LIST)
        let stationId:NSNumber = station.value(forKey: IDENTITY) as! NSNumber
        fetchRequest.predicate = NSPredicate(format:"%K == %@", IDENTITY, stationId)
        
        do {
            let result = try managedContext.fetch(fetchRequest)
            let object = result[0] as! NSManagedObject
            object.setValue(STATUS_VALUE_OFF_SERVICE, forKey: STATUS_VALUE)
            
            do {
                try managedContext.save()
            } catch  {
                print(error)
            }
           
        } catch  {
            print("Failed")
        }
    }
    
    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "BikeStationWithSwift")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}
