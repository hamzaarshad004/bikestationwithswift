
//
//  File.swift
//  BikeStationWithSwift
//
//  Created by ladmin on 04/06/2020.
//  Copyright © 2020 hamza. All rights reserved.
//

import Foundation
import CoreData

class Station: NSManagedObject {
    @NSManaged var identity: NSInteger
    @NSManaged var stationName: String
    @NSManaged var statusValue: String
    @NSManaged var availableDocks: NSInteger
    @NSManaged var statusKey: NSInteger
}
