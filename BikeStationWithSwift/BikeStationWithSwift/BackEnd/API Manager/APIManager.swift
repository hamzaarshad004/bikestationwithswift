//
//  APIManager.swift
//  BikeStationWithSwift
//
//  Created by ladmin on 04/06/2020.
//  Copyright © 2020 hamza. All rights reserved.
//

import UIKit
import Alamofire

class APIManager: NSObject {
    static let getAPIManager = APIManager()
    
    private override init(){}
    
    func getBikeStations(completionBlock: @escaping (NSDictionary) -> ()){
//        request.responseJSON { (data) in
//          print(data)
//        }
        
        let dataQueue = DispatchQueue(label: "dataQueue", attributes: .concurrent)
        dataQueue.async {
            let request = AF.request(STATION_LIST_API)
            request.responseJSON{
               response in
                switch response.result {
               case .success(let value as NSDictionary):
                   //print(value)
                   completionBlock(value)

               default:
                   fatalError("received non-dictionary JSON response")
               }
            }
        }
    }
}
