//
//  Constants.swift
//  BikeStationWithSwift
//
//  Created by ladmin on 04/06/2020.
//  Copyright © 2020 hamza. All rights reserved.
//

import Foundation

let STATION_LIST_API = "https://feeds.citibikenyc.com/stations/stations.json"

// CoreData Entity Name
let BIKE_LIST = "BikeList"

// Station Attribute Keys
let IDENTITY = "identity"
let STATION_NAME = "stationName"
let STATUS_VALUE = "statusValue"
let STATUS_KEY = "statusKey"
let AVAILABLE_DOCKS = "availableDocks"
let STATIONS_LIST_NAME = "stationBeanList"

// Station Attribute Value
let STATUS_VALUE_IN_SERVICE = "In Service"
let STATUS_VALUE_OFF_SERVICE = "Not In Service"

// NSURLSession
let ACCEPT_ALL_HEADERS = "Accept:*/*"
